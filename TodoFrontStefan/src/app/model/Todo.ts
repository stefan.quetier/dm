import {Statut} from "./Statut";

export interface Todo {
  id:Number,
  title:String,
  contenu:String,
  statut : Statut,
  date :String
}
