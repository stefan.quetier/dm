import {Component, Input} from '@angular/core';
import {Todo} from "../../model/Todo";
import {Statut} from "../../model/Statut";
import {ActivatedRoute} from "@angular/router";
import {TodoServices} from "../../services/todo.services";
@Component({
  selector: 'app-todo-details',
  templateUrl: './todo-details.component.html',
  styleUrls: ['./todo-details.component.css']
})
export class TodoDetailsComponent{
  todo!:Todo
  mode: String = "Edit"
  statut=Object.values(Statut)

  constructor(private userService: TodoServices, private route: ActivatedRoute) {
    if(history.state.user===null){
      this.todo=history.state.user;
    }else{
      const id_user: Number = Number(this.route.snapshot.paramMap.get("id"))
      this.userService.getTodosById(id_user).subscribe(todo => this.todo = todo)
    }
    for (let statut of this.statut) {
      console.log(statut)
    }
  }



  changeMode() {
    if (this.mode==="Edit") {
      this.mode = "Display"
    } else {
      this.mode = "Edit"
    }
  }

  updateTodo() {
    console.log("update")
    this.userService.updateTodo(this.todo).subscribe(()=>this.changeMode());
  }

}
