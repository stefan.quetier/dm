import {Todo} from "../../model/Todo";
import {TodoServices} from "../../services/todo.services";
import {Component} from "@angular/core";

@Component({
  selector: 'app-todo-list',
  templateUrl: './todo-list.component.html',
  styleUrls: ['./todo-list.component.css']
})
export class TodoListComponent{
  todos: Todo[]

  constructor(
    private todoService: TodoServices
  ) {
    this.todos = []
    todoService.getTodos()
      .subscribe(todos => this.todos = todos)
  }
}
