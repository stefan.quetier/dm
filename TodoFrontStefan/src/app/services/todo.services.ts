import {HttpClient, HttpHeaders} from "@angular/common/http";
import {catchError, Observable, of} from "rxjs";
import {Todo} from "../model/Todo";

export class TodoServices{
  private url = 'http://localhost:8080/todos'
  private httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };

  constructor(
    private http: HttpClient
  ) { }

  getTodos(): Observable<Todo[]> {
    return this.http.get<Todo[]>(this.url);
  }

  getTodosById(id: Number): Observable<Todo> {
    return this.http.get<Todo>(this.url+"/"+id);
  }

  updateTodo(user: Todo): Observable<any> {
    let updateUrl = this.url + "/" + user.id
    return this.http.put(updateUrl, user, this.httpOptions).pipe(catchError(this.handleError<any>('updateUser')))
  }

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      this.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }
}
