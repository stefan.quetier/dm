import {NgModule} from "@angular/core";
import {BrowserModule} from "@angular/platform-browser";
import {AppComponent} from "./app.component";
import {HttpClientModule} from "@angular/common/http";
import {AppRoutingModule} from './app-routing.module';
import {FormsModule} from "@angular/forms";
import {TodoListComponent} from "./todo/todo-list/todo-list.component";
import {TodoDetailsComponent} from "./todo/todo-details/todo-details.component";
import {TodoBlockComponent} from "./todo/todo-block/todo-block.component";


@NgModule({
  declarations: [
    AppComponent,
    TodoBlockComponent,
    TodoListComponent,
    TodoDetailsComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
