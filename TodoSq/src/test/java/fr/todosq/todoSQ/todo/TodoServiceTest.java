package fr.todosq.todoSQ.todo;

import fr.todosq.todoSQ.exceptions.ResourceAlreadyExistsException;
import fr.todosq.todoSQ.exceptions.ResourceNotFoundException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;


import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class TodoServiceTest {

    private TodoService todoService;

    private List<Todo> todos;

    @BeforeEach
    void setUp() {
        todos = new ArrayList<>(){{
            add(new Todo(1L,"tache1","Faire le todo",Statut.PASACCOMPLIE,"10/10/2022" ));
            add(new Todo(2L,"tache2","Finir le todo",Statut.ACCOMPLIE ,"10/10/2022"));
            add(new Todo(3L,"tache3","Faire le travail demandé",Statut.PASACCOMPLIE,"10/10/2022"));
        }};
        todoService = new TodoLocalService(todos) {
        };
    }

    @Test
    void whenCreating_ShouldReturnSame() {
        Todo toCreate = new Todo(5L,"tache3","Faire le travail demandé", Statut.ACCOMPLIE,"10/10/2022");

        assertEquals(toCreate, todoService.createTodo(toCreate));
    }

    @Test
    void whenCreatingWithSameId_shouldReturnEmpty() {
        Todo same_todo = todos.get(0);

        assertThrows(ResourceAlreadyExistsException.class, ()->todoService.createTodo(same_todo));
    }

    @Test
    void whenUpdatingTodo_shouldModifyTodo() {
        Todo initial_todo = todos.get(2);
        Todo new_todo = new Todo(initial_todo.getId(), "Updaté", initial_todo.getStatut());

        todoService.updateTodo(new_todo.getId(), new_todo);
        Todo updated_todo = todoService.getTodoById(initial_todo.getId());
        assertEquals(new_todo, updated_todo);
        assertTrue(todoService.getAll().contains(new_todo));
    }

    @Test
    void whenUpdatingTodoNonExisting_shouldThrowException() {
        Todo todo = todos.get(2);

        assertThrows(ResourceNotFoundException.class, ()->todoService.updateTodo(75L, todo));
    }


    @Test
    void whenDeletingTodoExisting_shouldNotBeInTodosAnymore() {
        Todo todo = todos.get(1);
        Long id = todo.getId();

        todoService.deleteTodo(id);
        assertFalse(todoService.getAll().contains(todo));
    }

    @Test
    void whenDeletingTodoNonExisting_shouldThrowException() {
        Long id = 68L;

        assertThrows(ResourceNotFoundException.class, ()->todoService.deleteTodo(id));
    }
}
