package fr.todosq.todoSQ.todo;

import com.fasterxml.jackson.databind.ObjectMapper;
import fr.todosq.todoSQ.exceptions.ExceptionHandlingAdvice;
import fr.todosq.todoSQ.exceptions.ResourceAlreadyExistsException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;

import java.util.ArrayList;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

@WebMvcTest
@ExtendWith(SpringExtension.class)
@Import(ExceptionHandlingAdvice.class)
@ContextConfiguration(classes = TodoController.class)
public class  TodoControllerTest {

    @Autowired
    MockMvc mockMvc;

    @MockBean(name = "jpa")
    TodoService todoService;

    private ArrayList<Todo> todos;

    @BeforeEach
    void setup(){
        todos =new ArrayList<>(){{
            add(new Todo(1L,"tache1","Faire le todo", Statut.ACCOMPLIE,"01/01/2022"));
            add(new Todo(2L,"tache2","Finir le todo", Statut.PASACCOMPLIE,"01/01/2023"));
            add(new Todo(3L,"tache3","Faire le travail demandé", Statut.ACCOMPLIE,"01/01/2024"));
        }};
    }

    @Test
    void whenGettingTodoId1L_shouldReturnSame() throws Exception{
        mockMvc.perform(get("/todos/1")
                .contentType(MediaType.APPLICATION_JSON)
        ).andExpect(status().isOk()
        ).andExpect(jsonPath("$.id", is(1L))
        ).andExpect(jsonPath("$.title", is("tache1"))
        ).andExpect(jsonPath("$.contenu", is("Faire le todo"))
        ).andExpect(jsonPath("$.statut", is("Accomplie"))
        ).andExpect(jsonPath("$.date", is("01/01/2024"))
        ).andReturn();
    }


    @Test
    void whenGettingTodoUnexistingId_should404() throws Exception {
        mockMvc.perform(get("/todos/4")
                .contentType(MediaType.APPLICATION_JSON)
        ).andExpect(status().isNotFound()
        ).andDo(print());
    }

    @Test
    void whenGeettingAll_sloudGet3todos() throws Exception {
        mockMvc.perform(get("/todos")
                .contentType(MediaType.APPLICATION_JSON)
        ).andExpect(status().isOk()
        ).andExpect(jsonPath("$",hasSize(3))
        ).andDo(print());
    }

    @Test
    void whenCreatingNew_shouldReturnLink_andShouldBeStatusCreated() throws Exception{
        Todo new_todo = new Todo(5L, "NouvelleTodo","contenu",Statut.PASACCOMPLIE,"01/01/2023");
        ArgumentCaptor<Todo> todo_received = ArgumentCaptor.forClass(Todo.class);
        when(todoService.createTodo(any())).thenReturn(new_todo);

        mockMvc.perform(post("/todos")
                .contentType(MediaType.APPLICATION_JSON)
                .content(new ObjectMapper().writeValueAsString(new_todo))
        ).andExpect(status().isCreated()
        ).andExpect(header().string("Location", "/todos/"+new_todo.getId())
        ).andDo(print());

        verify(todoService).createTodo(todo_received.capture());
        assertEquals(new_todo, todo_received.getValue());
    }

    @Test
    void whenCreatingWithExistingId_should404() throws Exception {
        when(todoService.createTodo(any())).thenThrow(ResourceAlreadyExistsException.class);
        mockMvc.perform(post("/todos")
                .contentType(MediaType.APPLICATION_JSON)
                .content(new ObjectMapper().writeValueAsString(this.todos.get(2)))
        ).andExpect(status().isConflict()
        ).andDo(print());
    }


    @Test
    void whenUpdating_shouldReceiveUserToUpdate() throws Exception {
        Todo initial_todo = todos.get(1);
        Todo updated_todo = new Todo(initial_todo.getId(),initial_todo.getTitle(),initial_todo.getContenu(),initial_todo.getStatut(),initial_todo.getDate());
        ArgumentCaptor<Todo> user_received = ArgumentCaptor.forClass(Todo.class);

        mockMvc.perform(put("/todos/"+initial_todo.getId())
                .contentType(MediaType.APPLICATION_JSON)
                .content(new ObjectMapper().writeValueAsString(initial_todo))
        ).andExpect(status().isNoContent());

        verify(todoService).updateTodo(anyLong(), user_received.capture());
        assertEquals(updated_todo, user_received.getValue());
    }


    @Test
    void whenDeletingExistingTodo_shouldCallServiceWithCorrectId() throws Exception {
        Long id =3L;

        mockMvc.perform(delete("/todos/"+id)
        ).andExpect(status().isNoContent()
        ).andDo(print());

        ArgumentCaptor<Long> id_received = ArgumentCaptor.forClass(Long.class);
        Mockito.verify(todoService).deleteTodo(id_received.capture());
        assertEquals(id, id_received.getValue());
    }
}
