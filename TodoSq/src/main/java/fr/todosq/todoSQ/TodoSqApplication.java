package fr.todosq.todoSQ;

import fr.todosq.todoSQ.todo.Statut;
import fr.todosq.todoSQ.todo.Todo;
import fr.todosq.todoSQ.todo.TodoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.util.ArrayList;
import java.util.List;

@SpringBootApplication
public class TodoSqApplication {

	@Autowired
	private TodoRepository todoRepository;


	public static void main(String[] args) {
		SpringApplication.run(TodoSqApplication.class, args);
	}

	@Bean
	public CommandLineRunner setUpBDD() {
		return (args) -> {
			Todo todo1 = new Todo(1L,"tache1","Faire le todo", Statut.ACCOMPLIE,"01/01/2022");
			Todo todo2 = new Todo(2L,"tache3","Todo en cours", Statut.PASACCOMPLIE,"01/01/2023");
			Todo todo3 = new Todo(3L,"tache3","Finir le todo ", Statut.ACCOMPLIE,"01/01/2024");
			List<Todo> todos = new ArrayList<>(){{
				add(todo1);
				add(todo2);
				add(todo3);

			}};
			todoRepository.saveAll(todos);
		};
    };



}
