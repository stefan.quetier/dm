package fr.todosq.todoSQ.todo;

import fr.todosq.todoSQ.exceptions.ResourceAlreadyExistsException;
import fr.todosq.todoSQ.exceptions.ResourceNotFoundException;
import fr.todosq.todoSQ.utils.LocalService;

import java.util.List;

public class TodoLocalService  extends LocalService<Todo, Long> implements TodoService {
    public TodoLocalService() {
        super();
        this.allValues.add(new Todo(1L, "TodoTest","test", Statut.ACCOMPLIE,"10/10/2022"));
    }

    public TodoLocalService(List<Todo> todos) {
        super(todos);
    }

    @Override
    protected String getIdentifier() {
        return "id";
    }

    @Override
    public List<Todo> getAll() {
        return super.getAll();
    }

    @Override
    public Todo getTodoById(Long id) {
        return this.getByIdentifier(id);
    }

    @Override
    public Todo createTodo(Todo newTodo) throws ResourceAlreadyExistsException {
        try {
            this.findById(newTodo.getId());
            throw new ResourceAlreadyExistsException("User", newTodo.getId());
        } catch (ResourceNotFoundException e) {
            this.allValues.add(newTodo);
            return newTodo;
        }
    }

    @Override
    public void updateTodo(Long id, Todo updatedUser) throws ResourceNotFoundException {
        IndexAndValue<Todo> found = this.findById(id);
        this.allValues.remove(found.index());
        this.allValues.add(found.index(), updatedUser);
    }

    @Override
    public void deleteTodo(Long id) throws ResourceNotFoundException {
        IndexAndValue<Todo> found = this.findById(id);
        this.allValues.remove(found.value());
    }

    public IndexAndValue<Todo> findById(Long id) {
        return super.findByProperty(id);
    }
}

