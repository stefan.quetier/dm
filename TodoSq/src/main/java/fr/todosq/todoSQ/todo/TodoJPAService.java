package fr.todosq.todoSQ.todo;

import fr.todosq.todoSQ.exceptions.ResourceAlreadyExistsException;
import fr.todosq.todoSQ.exceptions.ResourceNotFoundException;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@Qualifier("jpa")
public class TodoJPAService implements TodoService{
    private TodoRepository todoRepository;

    public TodoJPAService(TodoRepository todoRepository) {
        this.todoRepository = todoRepository;
    }

    @Override
    public List<Todo> getAll() {
        return todoRepository.findAll();
    }



    @Override
    public Todo getTodoById(Long id) {
        Optional<Todo> user = todoRepository.findById(id);
        if (user.isPresent()) {
            return user.get();
        } else {
            throw new ResourceNotFoundException("Todo", id);
        }
    }

    @Override
    public Todo createTodo(Todo newTodo) throws ResourceAlreadyExistsException {
        Long id = newTodo.getId();
        if(todoRepository.existsById(id)){
            throw new ResourceAlreadyExistsException("Todo",id);
        }else{
            return todoRepository.save(newTodo);
        }
    }

    @Override
    public void updateTodo(Long id, Todo updatedUser) throws ResourceNotFoundException {
        Optional<Todo> updatetodo = todoRepository.findById(id);
        if (updatetodo.isPresent()){
            Todo user = updatetodo.get();
            todoRepository.delete(user);
            todoRepository.save(updatedUser);
        }else {
            throw new ResourceNotFoundException("User",id);
        }
    }

    @Override
    public void deleteTodo(Long id) throws ResourceNotFoundException {
        if (todoRepository.existsById(id)){
            todoRepository.deleteById(id);
        }else {
            throw new ResourceNotFoundException("Todo",id);
        }
    }


}
