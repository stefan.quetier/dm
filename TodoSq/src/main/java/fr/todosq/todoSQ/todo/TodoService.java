package fr.todosq.todoSQ.todo;

import fr.todosq.todoSQ.exceptions.ResourceAlreadyExistsException;
import fr.todosq.todoSQ.exceptions.ResourceNotFoundException;

import java.util.List;

public interface TodoService {


    List<Todo> getAll();

    Todo getTodoById(Long id);

    Todo createTodo(Todo newTodo) throws ResourceAlreadyExistsException;

    void updateTodo(Long id, Todo updatedUser) throws ResourceNotFoundException;


    void deleteTodo(Long id) throws ResourceNotFoundException;
}
