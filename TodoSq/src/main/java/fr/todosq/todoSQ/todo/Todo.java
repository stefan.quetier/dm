package fr.todosq.todoSQ.todo;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;

import java.util.List;
import java.util.Objects;

@Entity
public class Todo {

    @Id
    private Long id;

    private String title;

    private String contenu;

    private Statut statut ;

    private String date;


    public Todo() {

    }

    public Todo(Long id, String title, String contenu, Statut statut, String date) {
        this.id = id;
        this.title = title;
        this.contenu = contenu;
        this.statut = statut;
        this.date=date;
    }

    public Todo(long l, String truc, Statut statut) {
    }

    public Todo(List<Todo> todos) {
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContenu() {
        return contenu;
    }

    public void setContenu(String contenu) {
        this.contenu = contenu;
    }

    public Statut getStatut() {
        return statut;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public void setStatut(Statut statut) {
        this.statut = statut;
    }

    @Override
    public boolean equals(Object obj) {
        if (getClass() != obj.getClass()){
            return false;
        }
        Todo comparing = (Todo) obj;
        return Objects.equals(this.id, comparing.getId()) &&
                this.title.equals(comparing.getTitle()) &&
                this.statut == comparing.statut;
    }
}
